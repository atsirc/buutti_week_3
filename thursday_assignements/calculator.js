((document) => {
    const calculator = document.getElementById('calculator');
    let input = [];

    calculator.addEventListener('click', (ev) => {
        const target = ev.target;
        let value = target.innerHTML;

        //jos on klikattu jossain muualla, älä ota sitä huomioon
        if (!target.classList.contains('btn')) {
            return;
        }
        
        //vaihtaa operaattorin jos viimeinen on operaattori (HUOM 2 * -1 ei toimi vaan muuttu 2-1)
        if (target.classList.contains('operator') && ['+', '-', '/', '*'].includes(input.at(-1))) {
            input.pop();
        }

        //jos on numero, niin liittää numeroa viimeisiin numeroon
        else if (target.classList.contains('number')) {
            const int = /\d/.test(input.at(-1)) ? input.pop() : '';
            value = int === '0' ? value : int + value;
        }

        // jos on decsimaali katsoo jos edeltävä on numero + huomio että ei listä pisteitä jos jo olemassa oleva desimaali
        else if (target.id === 'decimal') {
            const int = input.length > 0 && /\d/.test(input.at(-1)) ? input.pop() : '0';
            if (!int.includes('.')) {
                value = int + '.';
            } else {
                value = int;
            }
        }

        //laskee yhteen tai clearaa
        else if (target.classList.contains('equal') || target.id === 'clear') {
            if (target.classList.contains('equal')) {
                value = calculate(input.join(' '));
            } else {
                value = '0';
            }
            input = [];
        }

        input.push(value);
        console.log(input);
        reloadDisplay();
    })

    const reloadDisplay = () => {
        const display = document.getElementById('display');
        const contents = input.join(' ') || '0';
        display.innerHTML = contents;
    }

    const calculate = (string) => {
        // questionable technique
        return eval(string).toString();
    }

})(document);
