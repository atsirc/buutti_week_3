import axios from 'axios';

const todoUrl = 'https://jsonplaceholder.typicode.com/todos/';
const usersUrl = 'https://jsonplaceholder.typicode.com/users/'

// OSA 1 & 2
/*
let todos = await axios.get(todoUrl);
todos = todos.data.map(async todo => {
    const user = await axios.get(usersUrl+todo.userId)
    todo.user = user.data;
    return todo;
})

todos = await Promise.all(todos);
console.log(todos[0])
*/

// OSA 3??
/*
let todos = await axios.get(todoUrl)
                        .then(results => results.data.map(async item => {
                            const user = await axios.get(usersUrl+item.userId);
                            item.user = user.data;
                            return item}));
todos = await Promise.all(todos);
console.log(todos);
*/

// OSA 4 & 5
let todosResponse = await axios.get(todoUrl);
const todos = todosResponse.data;
let usersResponse = await axios.get(usersUrl);
const users = {};
//välttääkseen turhaa etsimistä
usersResponse.data.forEach(user => {
    users[user.id] = {
        name: user.name,
        username: user.username,
        email: user.email
    }
});

todos.forEach(todo => {
    todo['user'] = users[todo.userId]
})
console.log(todos)