const jokesDiv = document.getElementById('jokes')
const nerdyButton = document.getElementById('nerdy')
const randomButton = document.getElementById('random')
const allButton = document.getElementById('all')
const getRandom = (list) => {
    return list[Math.floor(Math.random()*list.length)]
}
nerdyButton.addEventListener('click', () => {
    const nerdyJokes = jokes.filter(el => el.categories.includes('nerdy')) 
    const joke = getRandom(nerdyJokes)
    jokesDiv.innerHTML = `<p><em>${joke.joke}</em></p>`
})

randomButton.addEventListener('click', () => {
    const joke = getRandom(jokes)
    jokesDiv.innerHTML = `<p><em>${joke.joke}</em></p>`
})

allButton.addEventListener('click', () => {
    jokesDiv.innerHTML =''
    jokes.forEach(el => {
        jokesDiv.innerHTML += `<p><em>${el.joke}</em><br/>Categories: ${el.categories.join(', ')}</p>`
    })
})