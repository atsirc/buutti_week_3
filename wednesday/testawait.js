const funktio = async(num) => {
    if (num !== 0) { 
        let promise = new Promise((resolve, _reject) => {
            setTimeout(() => resolve(num), 1000)
        });
        let result = await promise; // wait until the promise resolves
        console.log(result);
        funktio(num-1)
    }
}
funktio(5)