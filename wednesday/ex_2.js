const getValue = () => {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res({ value: Math.random() });
            rej({value: 'Random error'})
        }, Math.random() * 1500);
    });
};
//tämä vaatisi package.json missä type=module
//const result = await getValue()
//console.log(result)
getValue().then((success) => console.log(success.value)).catch(err => {console.log(err.value)})
getValue().then((success) => console.log(success.value)).catch(err => {console.log(err.value)})

const getValue2 = async () => {
    const result = await getValue()
    console.log(result.value)
};
getValue2()
getValue2()
