//kirjaimellinen tulkinta tehtävästä
const num = (n) => {
    let times = 0
    while (times !== n) {
        const num = n-times;
        const text = times === 0 ? 'Wait 1 second' : num === 1 ? 'Wait the last second..' : 'Wait another second' 
        const dots = '.'.repeat(times)
        setTimeout(() => {
            console.log(`${dots}${num} => ${text}`);
        }, times*1000)
        times++
    }
    setTimeout(()=>console.log('GO!'), n*1000)
}

num(5)

const value = new Promise((lyckades, misslyckades) => {
    const it_works = false;
    if (it_works) {
        lyckades("lyckadesd!");
    } else {
        misslyckades("misslyckadesed!")
    }
}) // useimmiten tästä .then .catch sen sijaan että sitä tehdänn tällee erikseen


//success on "lyckadesd!", err on "misslyckadesd!"
const newVal = value.then((success) => {
        console.log(`It returned ${success}`)
        //const next = new Promise((res, rej) => {}).then()
        // tässä voisi esim returnata jotain, tai sitten value = success
        return success
    }).catch((err) => {
        console.log(`It returned ${err}`)
        return err
    })

console.log(newVal)